#!/usr/bin/env bash

# Add this script to your wm startup file.

DIR="$HOME/.config/polybar/material"

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch the bar
# polybar -q main -c "$DIR"/config.ini &

if type "xrandr"; then
	for m in $(xrandr --query | grep " connected" | cut -d" " -f1); do
		MONITOR=$m polybar --reload main -c "$DIR"/config.ini &
		MONITOR=$m polybar --reload right -c "$DIR"/config.ini &
		polybar --reload center -c "$DIR"/config.ini &
	done
else
	polybar --reload main &
	polybar --reload right &
	polybar --reload center &
fi

# script wrote by @apetresc
